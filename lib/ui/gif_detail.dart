import 'package:flutter/material.dart';

import 'package:share/share.dart';

class GifDetail extends StatelessWidget {

  final Map _getData;

  GifDetail(this._getData);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_getData["title"]),
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: (){
              Share.share(_getData["images"]["fixed_height"]["url"]);
            },
          )
        ],
      ),
      backgroundColor: Colors.black,
      body: Center(
        child: Image.network(_getData["images"]["fixed_height"]["url"]),
      ),
    );
  }
}
